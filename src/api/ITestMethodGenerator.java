package api;

import com.intellij.openapi.editor.Editor;
import com.intellij.psi.PsiClass;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import org.jetbrains.annotations.Nullable;

import java.util.Set;

public interface ITestMethodGenerator {
    void createMethod(TestFramework descriptor,
                      PsiClass targetClass,
                      Editor editor,
                      @Nullable MemberInfo info, Set<String> existingNames);
}
