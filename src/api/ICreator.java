package api;

public interface ICreator {

    IPatternProvider getPattern();

}
