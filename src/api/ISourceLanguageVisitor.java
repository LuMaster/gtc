package api;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;

public interface ISourceLanguageVisitor {

    void collect(PsiClass aClass);

    void collect(PsiMethod method);

    PsiMethod getGeneratedTest(String methodName);

    IMockGenerator getMockGenerator();


}
