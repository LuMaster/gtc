package api;

import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.createTest.TestGenerator;
import forms.CreateTestsDialog;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITestGenerator extends TestGenerator{
    @Nullable
    PsiElement generateTest(Project var1, CreateTestsDialog var2);

    void addTestMethods(ITestMethodGenerator spockMethodGenerator, Editor editor,
                                PsiClass targetClass,
                                TestFramework descriptor,
                                Collection<MemberInfo> methods,
                                boolean generateBefore,
                                boolean generateAfter);

    ISourceLanguageVisitor getLanguageVisitor();
}
