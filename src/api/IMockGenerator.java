package api;

import com.intellij.lang.Language;
import com.intellij.openapi.project.Project;
import com.intellij.psi.JVMElementFactory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiType;

public interface IMockGenerator {

    Project getProject();

    JVMElementFactory getJVMElementFactory();

    Language getLanguage();

    PsiElement initializeEmptyConstructor(PsiType type, String[] modifiers);

}
