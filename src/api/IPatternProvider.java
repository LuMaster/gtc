package api;

public interface IPatternProvider {

    String getPattern();

}
