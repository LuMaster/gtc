package data.providers;

import api.IMockGenerator;
import api.ISourceLanguageVisitor;
import com.intellij.lang.Language;
import generators.spock.sourcevisitors.JavaLanguageVisitor;

public class SourceLanguageVisitorProvider {

    public static synchronized ISourceLanguageVisitor getInstance(IMockGenerator generator, Language language) {
        switch (language.getID()) {
            case "JAVA":
                return new JavaLanguageVisitor(generator);
            default:
                return null;
        }
    }

}
