package data.providers;

import api.IMockGenerator;
import api.ISourceLanguageVisitor;
import api.ITestGenerator;
import com.intellij.lang.Language;
import com.intellij.openapi.project.Project;
import generators.JavaMainTestGenerator;
import generators.spock.SpockClassGenerator;

public class TestCreatorProvider {

    public static synchronized ITestGenerator forLanguage(Project project, Language srcLanguage, Language testLanguage) {
        IMockGenerator mockGenerator = MockGeneratorProvider.getInstance(project, testLanguage);
        ISourceLanguageVisitor visitor = SourceLanguageVisitorProvider.getInstance(mockGenerator, srcLanguage);
        switch (testLanguage.getID()) {
            case "Groovy":
                return new SpockClassGenerator(visitor);
            default:
                return new JavaMainTestGenerator();
        }
    }

}
