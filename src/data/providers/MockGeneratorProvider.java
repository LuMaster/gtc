package data.providers;

import api.IMockGenerator;
import com.intellij.lang.Language;
import com.intellij.openapi.project.Project;
import generators.spock.mockgenerators.SpockMockGenerator;

public class MockGeneratorProvider {

    public static synchronized IMockGenerator getInstance(Project project, Language language) {
        switch (language.getID()) {
            case "Groovy":
                return new SpockMockGenerator(project, language);
            default:
                return null;
        }
    }


}
