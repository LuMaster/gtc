package generators.spock.sourcevisitors;

import api.IMockGenerator;
import api.ISourceLanguageVisitor;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.psi.util.PsiUtil;
import com.intellij.psi.util.TypeConversionUtil;
import generators.spock.SpockTest;
import generators.spock.SpockTestWrapper;

import java.util.*;

public class JavaLanguageVisitor extends JavaRecursiveElementVisitor implements ISourceLanguageVisitor {

    private boolean _hasConstructor = false;

    private Map<PsiElement, List<PsiElement>> _mapMockConnections = new HashMap<>(); //All apart from _mapArguments & _mapConstructorParam fields/variables/method calls we need to mock
    // Think about replacing PsiElement key for String
    private Map<PsiElement, List<PsiElement>> _mapArguments = new HashMap<>();
    private Map<PsiElement, List<PsiElement>> _mapConstructorParam = new HashMap<>();
    // Check if there is any util that provides information about assignment
    private Map<String, String> _lAssignedFromMock = new HashMap<>(); // If variable is assigned by mock and which one

    private final IMockGenerator _generator;
    private Set<String> existingNames;
    private Set<SpockTestWrapper> alreadyProcessedClass = new HashSet<>();

    private SpockTest _currentTest;
    private SpockTestWrapper _currentWrapper;

    public JavaLanguageVisitor(IMockGenerator generator) {
        _generator = generator;
    }


    private String createConstructorName(PsiClass containingClass) {
        String className = containingClass.getName();
        String classNameTrim = className == null ? "" : className.trim();
        int id = 1;
        String loopName = classNameTrim.toLowerCase();
        while (existingNames.contains(loopName)) {
            loopName = classNameTrim + (++id);
        }
        existingNames.add(loopName);
        return loopName;
    }


    @Override
    public void visitMethod(PsiMethod method) {
        if (method.isConstructor()) {
            processConstructor(method);
        } else {
            PsiParameter[] parameters = method.getParameterList().getParameters();
            for (PsiParameter parameter : parameters) {
                if (!_mapArguments.containsKey(parameter)) {
                    _mapArguments.put(parameter, new ArrayList<>());
                }
            }
        }
        super.visitMethod(method);
    }

    private void processConstructor(PsiMethod constructor) {
        PsiClass containingClass = constructor.getContainingClass();
        if (containingClass == null) {
            return;
        }
        PsiType psiType = PsiUtil.getTypeByPsiElement(containingClass);
        if (constructor.getParameterList().getParameters().length == 0) {
            _currentWrapper.updateMockConstructorSet(_generator.initializeEmptyConstructor(psiType, new String[0]));
        }
    }

    @Override
    public void visitForeachStatement(PsiForeachStatement statement) {
        PsiExpression iteratedValue = statement.getIteratedValue();
        if (iteratedValue == null) {
            return;
        }
        PsiType type = iteratedValue.getType();
        if (TypeConversionUtil.isPrimitiveAndNotNull(type)) {
            //Do sth
            // String defaultValueOfType = PsiTypesUtil.getDefaultValueOfType(type);
        }
        super.visitForeachStatement(statement);
    }

    /**
     * i < obj.getTab().length;
     *
     * @param expression
     */
    @Override
    public void visitBinaryExpression(PsiBinaryExpression expression) {
        super.visitBinaryExpression(expression);
    }

    /**
     * i++; eg. super.getValue()++;
     *
     * @param expression
     */
    @Override
    public void visitExpression(PsiExpression expression) {
        super.visitExpression(expression);
    }

    @Override
    public void visitExpressionStatement(PsiExpressionStatement statement) {
        super.visitExpressionStatement(statement);
    }

    @Override
    public void visitMethodCallExpression(PsiMethodCallExpression expression) {
        super.visitMethodCallExpression(expression);
    }

    @Override
    public void visitAssignmentExpression(PsiAssignmentExpression expression) {
        super.visitAssignmentExpression(expression);
    }

    @Override
    public void visitDeclarationStatement(PsiDeclarationStatement statement) {
        PsiElement[] declaredElements = statement.getDeclaredElements();
        for (PsiElement declaredElement : declaredElements) {

        }

        super.visitDeclarationStatement(statement);
    }

    @Override
    public void visitIfStatement(PsiIfStatement statement) {
        //TODO: Start visiting if statement
        super.visitIfStatement(statement);
    }

    @Override
    public void visitReturnStatement(PsiReturnStatement statement) {
        super.visitReturnStatement(statement);
    }

    @Override
    public void visitCodeBlock(PsiCodeBlock block) {
        super.visitCodeBlock(block);
    }

    public void visitLambdaExpression(PsiLambdaExpression expression) {
        super.visitLambdaExpression(expression);
    }

    public void visitClass(PsiClass aClass) {
        super.visitClass(aClass);
    }

    @Override
    public void collect(PsiClass aClass) {
        String qualifiedName = aClass.getQualifiedName();
        _currentWrapper = new SpockTestWrapper(qualifiedName);
        if (alreadyProcessedClass.contains(_currentWrapper)) {
            return;
        }
        alreadyProcessedClass.add(_currentWrapper);
        hasConstructor(aClass);
        visitClass(aClass);
    }

    private void hasConstructor(PsiClass aClass) {
        for (PsiMethod psiMethod : aClass.getMethods()) {
            if (psiMethod.isConstructor()) {
                _hasConstructor = true;
            }
        }
        if (!_hasConstructor) {
            PsiClassType classType = PsiTypesUtil.getClassType(aClass);
            _currentWrapper.updateMockConstructorSet(_generator.initializeEmptyConstructor(classType, new String[0]));
        }
    }

    private void collectDeep(PsiMethod method) {
        visitMethod(method);
    }

    @Override
    public void collect(PsiMethod method) {
        String methodName = method.getName();
        _currentTest = new SpockTest().withTestName(methodName).withLanguage(getMockGenerator().getLanguage());
        _currentWrapper.getTests().put(methodName, _currentTest);
        visitMethod(method);
    }

    @Override
    public PsiMethod getGeneratedTest(String methodName) {
        return _generator.getJVMElementFactory().createMethodFromText(_currentWrapper.getTest(methodName).toString(), null);
    }

    public IMockGenerator getMockGenerator() {
        return _generator;
    }
}
