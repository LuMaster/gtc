package generators.spock;

import com.intellij.psi.PsiElement;

import java.util.*;

public class SpockTestWrapper {

    private String _testClassName;
    private List<PsiElement> _mockConstructorSet = new ArrayList<>();
    private Map<String, SpockTest> _tests = new HashMap<>();

    public SpockTestWrapper(String _testClassName) {
        this._testClassName = _testClassName;
    }

    public void updateMockConstructorSet(Collection<PsiElement> _mockConstructorSet) {
        this._mockConstructorSet.addAll(_mockConstructorSet);
    }

    public void updateMockConstructorSet(PsiElement _mockConstructor) {
        this._mockConstructorSet.add(_mockConstructor);
    }

    public void updateTests(Map<String, SpockTest> _tests) {
        this._tests.putAll(_tests);
    }

    public Map<String, SpockTest> getTests() {
        return _tests;
    }

    public SpockTest getTest(String key) {
        SpockTest spockTest = _tests.get(key);
        spockTest.getGiven().add(chooseConstructorToInitialize());
        return spockTest;
    }

    private PsiElement chooseConstructorToInitialize() {
        return _mockConstructorSet.get(0);
    }

    public PsiElement getFirstConstructor() {
        return _mockConstructorSet.get(0);
    }
}
