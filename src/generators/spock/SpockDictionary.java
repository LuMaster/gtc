package generators.spock;

public class SpockDictionary {

    public static final String DEF_WORD = "def";

    public static final String L_PARENTH = "(";

    public static final String R_PARENTH = ")";

    public static final String PARENTHESIS = "()";

    public static final String L_BACE = "{";

    public static final String R_BRACE = "}";

    public static final String GIVEN = "given:";

    public static final String AND = "and:";

    public static final String WHEN = "when:";

    public static final String THEN = "then:";

    public static final String EXPECT = "expect:";

    public static final String WHERE = "where:";

    public static final String TABLE_SEPARATOR = "||";

    public static final String ANY = "_";

    public static final String ANY_AS = "_ as {0}";


    /**
     * {0} - def
     * {1} - method name
     * {2} - arguments
     * {3} - method body
     */
    public static final String METHOD_TEMPLATE = "{0} \"{1}\" ({2}) '{' {3} '}' ";

    public static final String VARIABLE_DEFINITION = "{0} {1} = new {2}";

    /**
     * {0} - CLASS TO CONSTRUCT NAME
     * (1) - EMPTY PARENTHESIS IN MOST CASES
     */
    public static final String NEW_EXPRESSION_DEF = "new {0}{1}";

    /**
     * {0} - Type
     * {1} - Var name
     * {2} - Initializer
     */
    public static final String DECLARATION = "{0} {1} = {2}";

    public static final String MOCK_INITIALIZER = "Mock()";

    /**
     * {0} - Class name
     */
    public static final String STUB_INITIALIZER = "Stub({0})";

    public static final String THEN_RETURN = " >> ";


}
