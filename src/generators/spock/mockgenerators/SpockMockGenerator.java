package generators.spock.mockgenerators;

import api.IMockGenerator;
import com.intellij.lang.Language;
import com.intellij.openapi.project.Project;
import com.intellij.psi.JVMElementFactory;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiType;
import generators.spock.SpockDictionary;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElementFactory;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.GrStatement;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.expressions.GrExpression;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.expressions.GrMethodCall;
import org.jetbrains.plugins.groovy.refactoring.DefaultGroovyVariableNameValidator;
import org.jetbrains.plugins.groovy.refactoring.GroovyNameSuggestionUtil;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This class should contain only methods that generate particular constructions(eg. Mocks)
 */
public class SpockMockGenerator implements IMockGenerator {

    private final GroovyPsiElementFactory _psiElementFactory;
    private final Project _project;
    private final Language _language;
    private Set<String> _restrictedNames = new HashSet<>();

    public SpockMockGenerator(Project project, Language language) {
        _project = project;
        _language = language;
        _psiElementFactory = GroovyPsiElementFactory.getInstance(project);
    }

    public void updateNames(Collection<String> collection) {
        _restrictedNames.addAll(collection);
    }

    public Project getProject() {
        return _project;
    }

    @Override
    public JVMElementFactory getJVMElementFactory() {
        return _psiElementFactory;
    }

    @Override
    public Language getLanguage() {
        return _language;
    }

    @Override
    public PsiElement initializeEmptyConstructor(PsiType type, String[] modifiers) {
        GrExpression emptyNewExpression = createEmptyNewExpression(type);
//        SuggestedNameInfo suggestedNames = null;
//        for (NameSuggestionProvider nameSuggestionProvider : NameSuggestionProvider.EP_NAME.getExtensions()) {
//            suggestedNames = nameSuggestionProvider.getSuggestedNames(emptyNewExpression, emptyNewExpression, names);
//        }
//        if (suggestedNames != null) {
//
//        }
        String[] strings = GroovyNameSuggestionUtil.suggestVariableNameByType(emptyNewExpression.getType(), new DefaultGroovyVariableNameValidator(emptyNewExpression, _restrictedNames));
        return _psiElementFactory.createVariableDeclaration(modifiers, emptyNewExpression, type, strings);
    }

    public GroovyPsiElementFactory getFactoryInstance() {
        return _psiElementFactory;
    }

    public GrStatement generateMock(PsiClass classToMock, String varName, Set<String> existingNames) {
        String name = classToMock.getName();

        return _psiElementFactory.createStatementFromText(name + " " + varName + SpockDictionary.MOCK_INITIALIZER, null);
    }

    private org.jetbrains.plugins.groovy.lang.psi.api.statements.expressions.GrExpression createEmptyNewExpression(PsiType type) {
        return _psiElementFactory.createExpressionFromText(MessageFormat.format(SpockDictionary.NEW_EXPRESSION_DEF, type.getPresentableText(), SpockDictionary.PARENTHESIS));
    }

    public GrMethodCall initializeMethodCall() {
        return null;
    }
}
