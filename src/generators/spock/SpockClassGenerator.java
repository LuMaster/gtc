package generators.spock;

import api.ISourceLanguageVisitor;
import api.ITestGenerator;
import api.ITestMethodGenerator;
import com.intellij.codeInsight.CodeInsightBundle;
import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.WriteAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.ex.IdeDocumentHistory;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.util.Computable;
import com.intellij.psi.JavaPsiFacade;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiManager;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.impl.source.PostprocessReformattingAspect;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.createTest.CreateTestDialog;
import com.intellij.util.IncorrectOperationException;
import forms.CreateTestsDialog;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.plugins.groovy.actions.GroovyTemplates;
import org.jetbrains.plugins.groovy.annotator.intentions.CreateClassActionBase;
import org.jetbrains.plugins.groovy.intentions.GroovyIntentionsBundle;
import org.jetbrains.plugins.groovy.lang.psi.GroovyPsiElementFactory;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.GrExtendsClause;
import org.jetbrains.plugins.groovy.lang.psi.api.statements.typedef.GrTypeDefinition;
import org.jetbrains.plugins.groovy.lang.psi.api.types.GrCodeReferenceElement;

import java.util.Collection;
import java.util.HashSet;

public class SpockClassGenerator implements ITestGenerator {

    private final ISourceLanguageVisitor _javaLanguageVisitor;

    public SpockClassGenerator(ISourceLanguageVisitor spockClassMockForJava) {
        _javaLanguageVisitor = spockClassMockForJava;
    }

    public ISourceLanguageVisitor getLanguageVisitor() {
        return _javaLanguageVisitor;
    }

    @Override
    public String toString() {
        return GroovyIntentionsBundle.message("intention.crete.test.groovy");
    }

    private static void addSuperClass(@NotNull GrTypeDefinition targetClass, @NotNull Project project, @Nullable String superClassName)
            throws IncorrectOperationException {
        if (superClassName == null) return;

        GroovyPsiElementFactory factory = GroovyPsiElementFactory.getInstance(project);

        PsiClass superClass = findClass(project, superClassName);
        GrCodeReferenceElement superClassRef;
        if (superClass != null) {
            superClassRef = factory.createCodeReferenceElementFromClass(superClass);
        } else {
            superClassRef = factory.createCodeReferenceElementFromText(superClassName);
        }
        GrExtendsClause extendsClause = targetClass.getExtendsClause();
        if (extendsClause == null) {
            extendsClause = (GrExtendsClause) targetClass.addAfter(factory.createExtendsClause(), targetClass.getNameIdentifierGroovy());
        }

        extendsClause.add(superClassRef);
    }

    @Nullable
    private static PsiClass findClass(Project project, String fqName) {
        GlobalSearchScope scope = GlobalSearchScope.allScope(project);
        return JavaPsiFacade.getInstance(project).findClass(fqName, scope);
    }

    public void addTestMethods(ITestMethodGenerator spockMethodGenerator, Editor editor,
                               PsiClass targetClass,
                               TestFramework descriptor,
                               Collection<MemberInfo> methods,
                               boolean generateBefore,
                               boolean generateAfter) throws IncorrectOperationException {
        final HashSet<String> existingNames = new HashSet<>();
        for (MemberInfo m : methods) {
            spockMethodGenerator.createMethod(descriptor, targetClass, editor, m, existingNames);
        }
    }

    private static void showErrorLater(final Project project, final String targetClassName) {
        ApplicationManager.getApplication().invokeLater(() -> Messages.showErrorDialog(project,
                CodeInsightBundle.message("intention.error.cannot.create.class.message", targetClassName),
                CodeInsightBundle.message("intention.error.cannot.create.class.title")));
    }

    @Nullable
    @Override
    public PsiElement generateTest(Project project, CreateTestsDialog d) {
        SpockMethodGenerator spockMethodGenerator = new SpockMethodGenerator(this, project, d.getTargetClass());
        return WriteAction.compute(() -> {
            final PsiClass test = (PsiClass) PostprocessReformattingAspect.getInstance(project).postponeFormattingInside(
                    (Computable<PsiElement>) () -> {
                        try {
                            IdeDocumentHistory.getInstance(project).includeCurrentPlaceAsChangePlace();
                            GrTypeDefinition targetClass = CreateClassActionBase.createClassByType(
                                    d.getTargetDirectory(),
                                    d.getClassName(),
                                    PsiManager.getInstance(project),
                                    null,
                                    GroovyTemplates.GROOVY_CLASS, true);
                            if (targetClass == null) return null;

                            addSuperClass(targetClass, project, d.getSuperClassName());

                            Editor editor = CodeInsightUtil.positionCursorAtLBrace(project, targetClass.getContainingFile(), targetClass);
                            addTestMethods(spockMethodGenerator, editor,
                                    targetClass,
                                    d.getSelectedTestFrameworkDescriptor(),
                                    d.getSelectedMethods(),
                                    d.shouldGeneratedBefore(),
                                    d.shouldGeneratedAfter());
                            return targetClass;
                        } catch (IncorrectOperationException e1) {
                            showErrorLater(project, d.getClassName());
                            return null;
                        }
                    });
            if (test == null) return null;
            JavaCodeStyleManager.getInstance(test.getProject()).shortenClassReferences(test);
            CodeStyleManager.getInstance(project).reformat(test);
            return test;
        });
    }

    /**
     * Do not use this method!!!
     */
    @Nullable
    @Override
    public PsiElement generateTest(final Project project, final CreateTestDialog d) {
        return null;
    }
}
