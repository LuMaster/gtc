package generators.spock;

public enum SpockLabels {

    GIVEN("given: "),

    AND("and: "),

    WHEN("when: "),

    THEN("then: "),

    WHERE("where: ");

    private String _label;

    SpockLabels(String label) {
        _label = label;
    }

    public String getLabel() {
        return _label;
    }

}
