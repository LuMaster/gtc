package generators.spock;

import api.ITestMethodGenerator;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiMember;
import com.intellij.psi.PsiMethod;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFramework;
import org.jetbrains.annotations.Nullable;

import java.util.Set;

public class SpockMethodGenerator implements ITestMethodGenerator {

    private final PsiClass _src;
    private final SpockClassGenerator _classGenerator;


    public SpockMethodGenerator(SpockClassGenerator classGenerator, Project project, PsiClass srcClass) {
        //TODO: Decide if takes src class or target class !
        _src = srcClass;
        _classGenerator = classGenerator;
    }


    public void createMethod(TestFramework descriptor,
                             PsiClass targetClass,
                             Editor editor,
                             @Nullable MemberInfo info, Set<String> existingNames) {
        if (info == null) return;
        PsiMember infoMember = info.getMember();
        if (!(infoMember instanceof PsiMethod)) return;

        PsiMethod infoMethod = (PsiMethod) infoMember;
        PsiClass containingClass = infoMethod.getContainingClass();

        if (containingClass == null) return;

        _classGenerator.getLanguageVisitor().collect(containingClass);
        _classGenerator.getLanguageVisitor().collect(infoMethod);
        PsiMethod generatedTest = _classGenerator.getLanguageVisitor().getGeneratedTest(infoMethod.getName());
        PsiDocumentManager.getInstance(targetClass.getProject()).commitDocument(editor.getDocument());
        System.out.println(generatedTest.getText());

        try {
            targetClass.add(generatedTest);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

}
