package generators.spock;

import com.intellij.lang.Language;
import com.intellij.psi.PsiElement;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class SpockTest {

    private String _testName;
    private Language _language;
    private List<PsiElement> _arguments;
    private List<PsiElement> _given;
    private List<PsiElement> _and;
    private List<PsiElement> _when;
    private List<PsiElement> _then;
    private List<PsiElement> _where;

    public SpockTest() {
        _testName = "";
        _arguments = new ArrayList<>();
        _given = new ArrayList<>();
        _and = new ArrayList<>();
        _when = new ArrayList<>();
        _then = new ArrayList<>();
        _where = new ArrayList<>();
    }

    public SpockTest withTestName(String name) {
        this._testName = name;
        return this;
    }

    public SpockTest withLanguage(Language language) {
        this._language = language;
        return this;
    }


    public List<PsiElement> getGiven() {
        return _given;
    }

    public List<PsiElement> getAnd() {
        return _and;
    }

    public List<PsiElement> getWhen() {
        return _when;
    }

    public List<PsiElement> getThen() {
        return _then;
    }

    public String getLabelBody(SpockLabels label) {
        switch (label) {
            case GIVEN:
                return getBody(label, _given);
            case AND:
                return getBody(label, _and);
            case WHEN:
                return getBody(label, _when);
            case THEN:
                return getBody(label, _then);
            case WHERE:
                return getBody(label, _where);
            default:
                return "";
        }
    }

    private String getBody(SpockLabels label, Collection<PsiElement> partElements) {
        String lineSeparator = "\n";
        StringBuilder builder = new StringBuilder().append(lineSeparator);
        builder.append(label.getLabel());
        for (PsiElement bodyElement : partElements) {
            builder.append(bodyElement.getText());
            builder.append(lineSeparator);
        }
        return builder.toString();
    }

    private String processArguments() {
        StringBuilder result = new StringBuilder();
        for (PsiElement argument : _arguments) {
            result.append(argument.getText());
            result.append(",");
        }
        String toString = result.toString();
        if (!toString.isEmpty()) {
            toString = toString.subSequence(0, toString.length() - 1).toString();
        }
        return toString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SpockTest)) return false;
        SpockTest spockTest = (SpockTest) o;
        return Objects.equals(_testName, spockTest._testName) &&
                Objects.equals(_arguments, spockTest._arguments) &&
                Objects.equals(_given, spockTest._given) &&
                Objects.equals(_and, spockTest._and) &&
                Objects.equals(_when, spockTest._when) &&
                Objects.equals(_then, spockTest._then) &&
                Objects.equals(_where, spockTest._where);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_testName, _arguments, _given, _and, _when, _then);
    }

    @Override
    public String toString() {
        String body = getLabelBody(SpockLabels.GIVEN) +
                getLabelBody(SpockLabels.AND) +
                getLabelBody(SpockLabels.WHEN) +
                getLabelBody(SpockLabels.WHERE) +
                getLabelBody(SpockLabels.THEN);
        return MessageFormat.format(SpockDictionary.METHOD_TEMPLATE, SpockDictionary.DEF_WORD, _testName, processArguments(), body);
    }
}
