package staticanalysis.inspections;

import com.intellij.codeInsight.daemon.GroupNames;
import com.intellij.codeInspection.AbstractBaseJavaLocalInspectionTool;
import com.intellij.codeInspection.CustomSuppressableInspectionTool;
import com.intellij.codeInspection.SuppressIntentionAction;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TestInspection extends AbstractBaseJavaLocalInspectionTool implements CustomSuppressableInspectionTool {

    TestInspection() {

    }

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return "CWE-100 inspection";
    }

    @Override
    @NotNull
    public String getShortName() {
        return "CWE-100";
    }

    @Override
    @Nls
    @NotNull
    public String getGroupDisplayName() {
        return GroupNames.SECURITY_GROUP_NAME;
    }

    @NotNull
    @Override
    public String[] getGroupPath() {
        String[] groupDisplayPath = super.getGroupPath();
        return new String[]{groupDisplayPath[0], "CWE"};
    }

    @Nullable
    @Override
    public SuppressIntentionAction[] getSuppressActions(@Nullable PsiElement psiElement) {
        return new SuppressIntentionAction[0];
    }

    @Override
    public boolean isInitialized() {
        return super.isInitialized();
    }

    @Nullable
    @Override
    public String getStaticDescription() {
        return "This is test inspection";
    }
}
