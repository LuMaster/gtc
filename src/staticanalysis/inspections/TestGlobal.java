package staticanalysis.inspections;

import com.intellij.analysis.AnalysisScope;
import com.intellij.codeInspection.*;
import com.intellij.codeInspection.reference.RefEntity;
import com.intellij.psi.PsiElement;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TestGlobal extends GlobalJavaInspectionTool implements CustomSuppressableInspectionTool {

    @Nullable
    @Override
    public CommonProblemDescriptor[] checkElement(@NotNull RefEntity refEntity, @NotNull AnalysisScope scope, @NotNull InspectionManager manager, @NotNull GlobalInspectionContext globalContext) {
        return super.checkElement(refEntity, scope, manager, globalContext);
    }

    @Nls
    @NotNull
    @Override
    public String getDisplayName() {
        return super.getDisplayName();
    }

    @Nullable
    @Override
    public SuppressIntentionAction[] getSuppressActions(@Nullable PsiElement psiElement) {
        return new SuppressIntentionAction[0];
    }
}
