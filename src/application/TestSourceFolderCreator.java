package application;

import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ContentEntry;
import com.intellij.openapi.roots.ModifiableRootModel;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.openapi.util.Computable;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.JavaDirectoryService;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiManager;
import com.intellij.psi.PsiPackage;

import java.io.File;
import java.util.StringTokenizer;

public class TestSourceFolderCreator {
    private Application _application = ApplicationManager.getApplication();
    private final Project _project;
    private final PsiManager _psiManager;
    private PsiPackage _testPsiPackage;

    public TestSourceFolderCreator(Project project) {
        _project = project;
        _psiManager = PsiManager.getInstance(project);
    }

    public void createTestsSourceFolder(Module moduleForSrc, Module moduleForTest, PsiPackage srcPackage) {
        Computable<PsiPackage> computable = () -> {
            VirtualFile fileByUrl = _project.getBaseDir();
            PsiDirectory baseDirectory = _psiManager.findDirectory(fileByUrl);
            if (baseDirectory == null) {
                return null;
            }
            // TODO:  1. Recognize different path for Spock Framework
            // TODO:  2. Create pattern recognition for build systems or other modules (src folder in other module)
            // TODO:  3. Recognize if modules for src and tests are different and generate proper (or take from module test) src folder
            PsiDirectory tests = baseDirectory.findSubdirectory("tests");
            baseDirectory = tests == null ? baseDirectory.createSubdirectory("tests") : baseDirectory;

           //TODO: Temporary solution for spock subfolder
//            if (!_additionalSubFolder.isEmpty()) {
//                PsiDirectory sub = baseDirectory.findSubdirectory(_additionalSubFolder);
//                baseDirectory = sub == null ? baseDirectory.createSubdirectory(_additionalSubFolder) : baseDirectory;
//            }

            VirtualFile baseFile = baseDirectory != null ? baseDirectory.getVirtualFile() : null;
            if (baseDirectory == null) {
                return null;
            }
            String canonicalPath = baseFile.getCanonicalPath();
            if (canonicalPath == null) {
                return null;
            }
            VirtualFile virtualFile = LocalFileSystem.getInstance().refreshAndFindFileByIoFile(new File(canonicalPath));
            if (virtualFile == null) {
                return null;
            }
            ModifiableRootModel modifiableModel = ModuleRootManager.getInstance(moduleForTest).getModifiableModel();
            for (ContentEntry contentEntry : modifiableModel.getContentEntries()) {
                contentEntry.addSourceFolder(virtualFile.getUrl(), true);
                modifiableModel.commit();
            }
            final StringTokenizer tokenizer = new StringTokenizer(srcPackage.getQualifiedName(), ".");
            while (tokenizer.hasMoreTokens()) {
                String packagePart = tokenizer.nextToken();
                PsiDirectory subdirectory = baseDirectory.findSubdirectory(packagePart);
                if (subdirectory == null) {
                    baseDirectory.checkCreateSubdirectory(packagePart);
                    subdirectory = baseDirectory.createSubdirectory(packagePart);
                }
                baseDirectory = subdirectory;
            }
            return JavaDirectoryService.getInstance().getPackage(baseDirectory);
        };
        _testPsiPackage = _application.runWriteAction(computable);
    }

    public PsiPackage getCreatedPackage() {
        return _testPsiPackage;
    }

}
