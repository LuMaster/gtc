package application;

import api.ITestGenerator;
import com.intellij.codeInsight.CodeInsightBundle;
import com.intellij.codeInsight.CodeInsightUtil;
import com.intellij.codeInsight.FileModificationService;
import com.intellij.codeInsight.TestFrameworks;
import com.intellij.codeInsight.hint.HintManager;
import com.intellij.ide.util.PsiClassListCellRenderer;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.CommandProcessor;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.module.ModuleUtilCore;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.psi.*;
import com.intellij.refactoring.util.classMembers.MemberInfo;
import com.intellij.testIntegration.TestFinderHelper;
import com.intellij.testIntegration.TestFramework;
import com.intellij.testIntegration.createTest.CreateTestAction;
import com.intellij.testIntegration.createTest.MissedTestsDialog;
import com.intellij.ui.components.JBList;
import com.intellij.util.containers.ContainerUtil;
import data.collectors.TestDirectoryCollector;
import data.providers.TestCreatorProvider;
import forms.CreateTestsDialog;
import generators.spock.SpockClassGenerator;
import generators.spock.SpockMethodGenerator;

import java.util.Collection;

public class GenerateEnhanced extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        // 1 .Get element to process section
        // 2. Recognize language of source
        // 3. Recognize framework to generate tests and its language
        Project currentProject = anActionEvent.getData(PlatformDataKeys.PROJECT);
        if (currentProject == null) {
            return;
        }

        SelectionProvider selectionProvider = ProjectInitializer._selectionProviderMap.get(currentProject);
        PsiElement lastSelectedElement = selectionProvider.getLastSelectedElement();
        if (lastSelectedElement == null) {
            Messages.showInfoMessage(currentProject, "Not supported object for generating tests.", "Not Supported Object");
            return;
        }
        PsiClass srcClass = getPsiClass(lastSelectedElement);
        if (srcClass == null) {
            return;
        }
        Project project = srcClass.getProject();

        //Check if element is ok section
        if (!CreateTestAction.isAvailableForElement(srcClass)) {
            return;
        }

        final Collection<PsiElement> testClasses = TestFinderHelper.findTestsForClass(srcClass);

        // Generate tests for non existing class section
        if (testClasses.isEmpty()) {
            // Find proper module if other that src module and create source test folder if there is no such in current module
            Module moduleForSrc = ModuleUtilCore.findModuleForPsiElement(srcClass);
            if (moduleForSrc == null) {
                Messages.showErrorDialog("There is no source module for this file.", "No Source Module");
                return;
            }
            Module moduleForTests = CreateTestAction.suggestModuleForTests(project, moduleForSrc);
            PsiPackage srcPackage = JavaDirectoryService.getInstance().getPackage(srcClass.getContainingFile().getContainingDirectory());
            if (srcPackage == null) {
                return;
            }
            if (TestDirectoryCollector.computeSuitableTestRootUrls(moduleForTests).isEmpty()) {
                // 1. Call generating new test source
                TestSourceFolderCreator sourceFolderCreator = new TestSourceFolderCreator(project);
                sourceFolderCreator.createTestsSourceFolder(moduleForSrc, moduleForTests, srcPackage);
                PsiPackage createdPackage = sourceFolderCreator.getCreatedPackage();
                if (createdPackage == null) {
                    return;
                }
                // 2. Generate tests in passed package
                temporaryGeneratingMethod(project, srcClass, createdPackage, moduleForTests);
            } else {
                temporaryGeneratingMethod(project, srcClass, srcPackage, moduleForTests);
            }
            return;
        }
        Editor editor = anActionEvent.getData(PlatformDataKeys.EDITOR);
        if (editor == null) {
            return;
        }
        // Generate tests for one existing class section
        if (testClasses.size() == 1) {
            generateMissedTests((PsiClass) ContainerUtil.getFirstItem(testClasses), srcClass, editor);
            return;
        }

        // Generate tests for multiple existing class section
        final JBList list = new JBList(testClasses);
        list.setCellRenderer(new PsiClassListCellRenderer());
        final PsiClass finalSrcClass = srcClass;
        JBPopupFactory.getInstance().createListPopupBuilder(list)
                .setItemChoosenCallback(() -> generateMissedTests((PsiClass) list.getSelectedValue(), finalSrcClass, editor))
                .setTitle("Choose Test")
                .createPopup().showInBestPositionFor(editor);
    }

    private PsiClass getPsiClass(PsiElement lastSelectedElement) {
        if (lastSelectedElement instanceof PsiMethod) {
            return ((PsiMethod) lastSelectedElement).getContainingClass();
        }
        if (lastSelectedElement instanceof PsiClass) {
            return ((PsiClass) lastSelectedElement);
        }
        if (lastSelectedElement instanceof PsiJavaFile) {
            //TODO: Need better way ?
            return ((PsiJavaFile) lastSelectedElement).getClasses()[0];
        }
        return null;
    }

    public void temporaryGeneratingMethod(Project project, PsiClass srcClass, PsiPackage srcPackage, Module moduleForTests) {
        DumbService.getInstance(project).runWhenSmart(() -> {
            final CreateTestsDialog d = new CreateTestsDialog(project, "Create Test", srcClass, srcPackage, moduleForTests);
            if (!d.showAndGet()) {
                return;
            }
            CommandProcessor.getInstance().
                    executeCommand(
                            project,
                            () -> {
                                TestFramework framework = d.getSelectedTestFrameworkDescriptor();
                                final ITestGenerator generator = TestCreatorProvider.forLanguage(project, srcClass.getLanguage(), framework.getLanguage());
                                DumbService.getInstance(project).withAlternativeResolveEnabled(() -> {
                                    PsiElement psiElement = generator.generateTest(project, d);
                                });
                            },
                            CodeInsightBundle.message("intention.create.test"),
                            this);
        });
    }

    private void generateMissedTests(PsiClass testClass, PsiClass srcClass, Editor srcEditor) {
        if (testClass != null) {
            final TestFramework framework = TestFrameworks.detectFramework(testClass);
            if (framework != null) {
                final Project project = testClass.getProject();
                final Editor editor = CodeInsightUtil.positionCursorAtLBrace(project, testClass.getContainingFile(), testClass);
                if (!FileModificationService.getInstance().preparePsiElementsForWrite(testClass)) return;
                final MissedTestsDialog dialog = new MissedTestsDialog(project, srcClass, testClass, framework);
                if (dialog.showAndGet()) {
                    final ITestGenerator generator = TestCreatorProvider.forLanguage(project, srcClass.getLanguage(), framework.getLanguage());
                    Collection<MemberInfo> selectedMethods = dialog.getSelectedMethods();
                    SpockMethodGenerator spockMethodGenerator = new SpockMethodGenerator((SpockClassGenerator) generator, project, srcClass);
                    WriteCommandAction.runWriteCommandAction(project, () -> generator.addTestMethods(spockMethodGenerator, editor, testClass, framework, selectedMethods, false, false));
                }
            } else {
                HintManager.getInstance().showErrorHint(srcEditor, "Failed to detect test framework for " + testClass.getQualifiedName());
            }
        }
    }
}
