package application;

import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.project.ProjectManager;
import com.intellij.openapi.project.VetoableProjectManagerListener;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ProjectInitializer implements ProjectComponent {

    static Map<Project, SelectionProvider> _selectionProviderMap = new HashMap<>();
    private Map<Project, ScheduledExecutorService> _serviceHashMap = new HashMap<>();

    @Override
    public void initComponent() {
        System.out.println("Initializing component");
        Project[] openProjects = ProjectManager.getInstance().getOpenProjects();
        ProjectManager projectManager = ProjectManager.getInstance();

        projectManager.addProjectManagerListener(new VetoableProjectManagerListener() {
            @Override
            public boolean canClose(@NotNull Project project) {
                return true;
            }

            @Override
            public void projectOpened(Project project) {
                SelectionProvider selectionProvider = new SelectionProvider(project);
                selectionProvider.initFileEditorListener();
                ScheduledExecutorService _scheduler = Executors.newScheduledThreadPool(1);
                _scheduler.scheduleAtFixedRate(selectionProvider.new ProjectTreeAndEditorChecker(), 1000, 400, TimeUnit.MILLISECONDS);
                _serviceHashMap.put(project, _scheduler);
                _selectionProviderMap.put(project, selectionProvider);
            }

            @Override
            public void projectClosed(Project project) {
                _serviceHashMap.get(project).shutdown();
                _selectionProviderMap.get(project).getMessageBus().connect().disconnect();
            }
        });
    }

    @Override
    public void projectOpened() {


    }

    @Override
    public void projectClosed() {

    }
}
