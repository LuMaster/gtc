package application;

import com.intellij.codeInspection.InspectionToolProvider;
import com.intellij.openapi.components.ApplicationComponent;
import org.jetbrains.annotations.NotNull;
import staticanalysis.inspections.TestInspection;

public class InspectionRegistration implements InspectionToolProvider, ApplicationComponent {
    @NotNull
    @Override
    public Class[] getInspectionClasses() {
        return new Class[]{
                TestInspection.class
        };
    }

    @NotNull
    @Override
    public String getComponentName() {
        return "InspectionRegistration";
    }
}
