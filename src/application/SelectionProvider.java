package application;

import com.intellij.ide.projectView.ProjectView;
import com.intellij.ide.projectView.impl.AbstractProjectViewPane;
import com.intellij.openapi.application.Application;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.LogicalPosition;
import com.intellij.openapi.editor.event.CaretEvent;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.FileEditorManagerEvent;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindowId;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.psi.*;
import com.intellij.util.messages.MessageBus;
import org.jetbrains.plugins.groovy.lang.psi.GroovyFile;

public class SelectionProvider {
    private final Project _project;
    private final MessageBus _messageBus;
    private PsiElement lastSelectedElement;

    public SelectionProvider(Project project) {
        _project = project;
        _messageBus = project.getMessageBus();
    }

    public Project getProject() {
        return _project;
    }

    public MessageBus getMessageBus() {
        return _messageBus;
    }

    public PsiElement getLastSelectedElement() {
        if (lastSelectedElement != null) {
            lastSelectedElement = getProperParent(lastSelectedElement);
        }
        return lastSelectedElement;
    }

    private PsiElement getProperParent(PsiElement lastSelectedElement) {
        if (lastSelectedElement instanceof PsiMethod || lastSelectedElement instanceof PsiFile || lastSelectedElement instanceof PsiClass) {
            return lastSelectedElement;
        }
        PsiElement parent = lastSelectedElement.getParent();
        while (parent != null) {
            if (parent instanceof PsiMethod) {
                return parent;
            }
            if (parent instanceof PsiClass) {
                return parent;
            }
            if (parent instanceof PsiJavaFile || parent instanceof GroovyFile) {
                return parent;
            }
            parent = parent.getParent();
        }
        return null;
    }

    public void initFileEditorListener() {
        _messageBus.connect().subscribe(FileEditorManagerListener.FILE_EDITOR_MANAGER, new FileEditorManagerListener() {
            @Override
            public void fileOpened(FileEditorManager source, VirtualFile file) {
                executeCaretChecker(source);
            }

            @Override
            public void fileClosed(FileEditorManager source, VirtualFile file) {
                lastSelectedElement = null;
            }

            @Override
            public void selectionChanged(FileEditorManagerEvent event) {
                executeCaretChecker(event.getManager());
            }

            private void executeCaretChecker(FileEditorManager manager) {
                Editor selectedTextEditor = manager.getSelectedTextEditor();
                if (selectedTextEditor == null) {
                    return;
                }
                CaretModel caretModel = selectedTextEditor.getCaretModel();
                LogicalPosition logicalPosition = caretModel.getLogicalPosition();
                CaretEvent caretEvent = new CaretEvent(selectedTextEditor, caretModel.getCurrentCaret(), logicalPosition, logicalPosition);
                lastSelectedElement = getCurrentCaretElement(selectedTextEditor, caretEvent);
            }
        });
    }

    private PsiElement getCurrentCaretElement(Editor editor, CaretEvent caretEvent) {
        Project project = editor.getProject();
        Caret caret = caretEvent.getCaret();
        if (project == null || caret == null) {
            return null;
        }
        PsiManager psiManager = PsiManager.getInstance(project);
        VirtualFile virtualFile = FileDocumentManager.getInstance().getFile(editor.getDocument());
        if (virtualFile == null) {
            return null;
        }
        PsiFile psiFile = psiManager.findFile(virtualFile);
        if (psiFile == null) {
            return null;
        }
        return psiFile.getViewProvider().findElementAt(caret.getOffset());
    }

    class ProjectTreeAndEditorChecker implements Runnable {

        private final ToolWindowManager _manager;
        private int _previousCaretPosition = -1;

        ProjectTreeAndEditorChecker() {
            _manager = ToolWindowManager.getInstance(_project);
        }

        @Override
        public void run() {
            // Access is allowed from event dispatch thread only in _manager.getToolWindow(ToolWindowId.PROJECT_VIEW).isActive() method
            Application application = getApplication();
            application.invokeLater(() -> getApplication().runReadAction(() -> {
                if (_manager.getToolWindow(ToolWindowId.PROJECT_VIEW).isActive()) {
                    Object[] selectedElements = getSelectedElements();
                    if (selectedElements == null || selectedElements.length == 0 || selectedElements.length > 1) {
                        return;
                    }
                    Object selectedElement = selectedElements[0];
                    if (!(selectedElement instanceof PsiElement)) {
                        return;
                    }
                    if (selectedElement.equals(lastSelectedElement)) {
                        return;
                    }
                    lastSelectedElement = (PsiElement) selectedElement;
                } else {
                    Editor selectedTextEditor = FileEditorManager.getInstance(_project).getSelectedTextEditor();
                    if (selectedTextEditor == null) {
                        return;
                    }
                    CaretModel caretModel = selectedTextEditor.getCaretModel();
                    LogicalPosition logicalPosition = caretModel.getLogicalPosition();
                    CaretEvent caretEvent = new CaretEvent(selectedTextEditor, caretModel.getCurrentCaret(), logicalPosition, logicalPosition);

                    if (caretModel.getOffset() == _previousCaretPosition) {
                    } else {
                        _previousCaretPosition = caretModel.getOffset();
                        lastSelectedElement = getCurrentCaretElement(selectedTextEditor, caretEvent);
                    }
                }
            }));

        }

        protected Application getApplication() {
            return ApplicationManager.getApplication();
        }

        Object[] getSelectedElements() {
            AbstractProjectViewPane currentPanel = getCurrentProjectViewPane();
            if (currentPanel == null) {
                return null;
            }

            return currentPanel.getSelectedElements();
        }

        AbstractProjectViewPane getCurrentProjectViewPane() {
            return ProjectView.getInstance(_project).getCurrentProjectViewPane();
        }
    }
}
